import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { TravelDestination } from './TravelDestination.model';
import { HttpClientModule } from '@angular/common/http'

//State
export interface TravelDestinationState{
    items: TravelDestination[];
    loading: boolean;
    favorite: TravelDestination;
}

export function initializeTravelDestinationState(){
    return {
        items: [],
        loading: false,
        favorite: null
    }
}

//Actions

export enum TravelDestinationsActionTypes {
    NEW_DESTINATION = '[Travel Destinations] New',
    FAVORITE_DESTINATION = '[Travel Destinations] Favorite',
    VOTE_UP = '[Travel Destinations] Vote Up',
    VOTE_DOWN = '[Travel Destinations] Vote Down', 
    INIT_MY_DATA = '[Travel Destinations] Init My Data'
}

export class NewDestinationAction implements Action {
    type = TravelDestinationsActionTypes.NEW_DESTINATION;
    constructor(public destination: TravelDestination){}
}

export class SelectedFavoriteAction implements Action {
    type = TravelDestinationsActionTypes.FAVORITE_DESTINATION;
    constructor(public destination: TravelDestination){}
}

export class VoteUpAction implements Action {
    type = TravelDestinationsActionTypes.VOTE_UP;
    constructor(public destination: TravelDestination){}
}

export class VoteDownAction implements Action {
    type = TravelDestinationsActionTypes.VOTE_DOWN;
    constructor(public destination: TravelDestination){}
}

export class InitMyDataAction implements Action {
    type = TravelDestinationsActionTypes.INIT_MY_DATA;
    constructor(public destination: string[]){}
}

export type TravelDestinationsActions = NewDestinationAction | SelectedFavoriteAction | VoteUpAction | VoteDownAction | InitMyDataAction;

//Reducers

export function reducerTravelDestination (
    state: TravelDestinationState,
    action: TravelDestinationsActions
): TravelDestinationState {
    switch(action.type){
        case TravelDestinationsActionTypes.INIT_MY_DATA:{
            const destinations: string[] = (action as InitMyDataAction).destination;
            return {
                ...state, items: destinations.map((d) => new TravelDestination(d, ''))
            };
        }
        case TravelDestinationsActionTypes.NEW_DESTINATION:{
            return {
                ...state, items: [...state.items, (action as NewDestinationAction).destination]
            };
        }
        case TravelDestinationsActionTypes.FAVORITE_DESTINATION:{
            state.items.forEach(x => x.setSelected(false));
            const favorite: TravelDestination = (action as SelectedFavoriteAction).destination;
            favorite.setSelected(true);
            return{
                ...state, favorite: favorite
            };
        }
        case TravelDestinationsActionTypes.VOTE_UP:{
            const entity: TravelDestination = (action as VoteUpAction).destination;
            entity.voteUp();
            return{ ...state };
        }
        case TravelDestinationsActionTypes.VOTE_DOWN:{
            const entity: TravelDestination = (action as VoteDownAction).destination;
            entity.voteDown();
            return{ ...state };
        }
    }
    return state;
}

//Effects

@Injectable()
export class TravelDestinationEffects {
    @Effect()
    newAdd$: Observable<Action> = this.actions$.pipe(
        ofType(TravelDestinationsActionTypes.NEW_DESTINATION),
        map((action: NewDestinationAction) => new SelectedFavoriteAction(action.destination))
    );
    constructor(private actions$: Actions){}
}