import { v4 as uuid } from 'uuid';

export class TravelDestination {
    
    selected: boolean;
    id = uuid();
    services: string[];

    constructor(public name: string, public urlImage: string, public votes: number = 0) { 
        this.services = ['huevitos','arroz'];
    }

    setSelected(item: boolean) {
        this.selected = item;
    }

    isSelected(){
        return this.selected;
    }

    voteUp(){
        this.votes++;
    }

    voteDown(){
        this.votes--;
    }
}