import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReservationApiClientService {

  constructor() { }

  GetAll(){
    return [{ id: 1, name: 'Uno' }, { id: 2, name: 'Dos' }];
  }
}
