import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  Login(user: string, password: string): boolean {
    if (user === 'user' && password === 'password') {
      localStorage.setItem('username', user);
      return true;
    }
    return false;
  }

  LogOut(): any {
    localStorage.removeItem('username');
  }

  GetUser(): any {
    return localStorage.getItem('username');
  }

  IsLoggedIn(): boolean {
    return this.GetUser() !== null;
  }
}
