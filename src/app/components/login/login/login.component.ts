import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorMessage: string;

  constructor(public authService: AuthService) {
    this.errorMessage = '';
   }

  ngOnInit(): void {
  }

  Login(username: string, password: string): boolean{
    this.errorMessage = '';
    if(!this.authService.Login(username, password)){
      this.errorMessage = 'Credenciales incorrectas';
      setTimeout(function(){
        this.errorMessage = '';
      }.bind(this), 2500);
    }
    return false;
  }

  LogOut():boolean{
    this.authService.LogOut();
    return false;
  }

}
