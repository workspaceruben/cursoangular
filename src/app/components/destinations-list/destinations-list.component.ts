import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TravelDestination } from './../../models/TravelDestination.model';
import { DestinationApiClient } from './../../models/DestinationApiClient.model';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';

@Component({
  selector: 'app-destinations-list',
  templateUrl: './destinations-list.component.html',
  styleUrls: ['./destinations-list.component.css'],
  providers: [DestinationApiClient]
})
export class DestinationsListComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<TravelDestination>;
  updates: string[];
  all;

  constructor(public destinationsApiClient: DestinationApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinations.favorite).subscribe(data => {
      if(data != null){
        this.updates.push('Se ha elegido a ' + data.name);
      }
    });
    store.select(state => state.destinations.items).subscribe(items => this.all = items);
   }

  ngOnInit(): void{
  }

  Save(entity: TravelDestination) {
    this.destinationsApiClient.Add(entity);
    this.onItemAdded.emit(entity);
  }

  DestinationSelected(destination: TravelDestination) {
    this.destinationsApiClient.SelectTravelDestination(destination);
  }
}
