import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../app.module';
import { TravelDestination } from './../../models/TravelDestination.model';
import { VoteDownAction, VoteUpAction } from './../../models/TravelDestinationState.model';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-travel-destination',
  templateUrl: './travel-destination.component.html',
  styleUrls: ['./travel-destination.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor: 'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNoFavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito', [
        animate('1s')
      ]),
    ])
  ]
})

export class TravelDestinationComponent implements OnInit {
  @Input("idx") position: number;
  @Input() destination: TravelDestination;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<TravelDestination>;

  constructor(private store: Store<AppState>) { 
    this.onClicked = new EventEmitter();
  }

  ngOnInit(): void {
  }

  Go(){
    this.onClicked.emit(this.destination);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destination));
    return false;
  }

  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destination));
    return false;
  }

}
