import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounce, distinctUntilChanged, switchMap, debounceTime } from 'rxjs/operators';
import { ajax, AjaxResponse} from 'rxjs/ajax';
import { TravelDestination } from './../../models/TravelDestination.model';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-travel-destination',
  templateUrl: './form-travel-destination.component.html',
  styleUrls: ['./form-travel-destination.component.css']
})
export class FormTravelDestinationComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<TravelDestination>;
  formGroup: FormGroup;
  minLong = 3;
  searchResults: string[];

  constructor(formBuilder: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.formGroup = formBuilder.group({
      name: ['', Validators.compose([Validators.required, this.nameValidatorByParameter(this.minLong)])], urlImage: ['']
    });

    this.formGroup.valueChanges.subscribe((form: any) => {
      console.log('cambio en el formulario: ', form);
    })
   }

  ngOnInit() {
    const nameElement = <HTMLInputElement>document.getElementById('name');
    fromEvent(nameElement, 'input').pipe(map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
    filter(text => text.length > 2),
    debounceTime(200),
    distinctUntilChanged(),
    switchMap((text: string) => ajax(this.config.apiEndPoint + '/cities?q=' + text)))
    .subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
  }

  Save(name: string, urlImage: string): boolean{
    const destination = new TravelDestination(name, urlImage);
    this.onItemAdded.emit(destination);
    return false;
  }

  nameValidator(control: FormControl): { [s: string]: boolean } {
    const long = control.value.toString().trim().length;
    if(long > 0 && long < 5){
      return { invalidName: true };
    }
    return null;
  }

  nameValidatorByParameter(minLong: number): ValidatorFn{
    return(control: FormControl): { [s: string]: boolean } | null => {
      const long = control.value.toString().trim().length;
      if(long > 0 && long < minLong){
        return { minLongName: true };
      }
      return null;
    }
  }
}
