import { Directive, OnInit, OnDestroy } from '@angular/core';

@Directive({
  selector: '[appEspiame]'
})
export class SpyMeDirective implements OnInit, OnDestroy {
  static nextId = 0;
  log = (msg: string) => console.log(`Evento #${SpyMeDirective.nextId++} ${msg}`);
  ngOnInit() { this.log(`###############*************** onInit`); }
  ngOnDestroy() { this.log(`###############*************** onDestroy`); }
}

